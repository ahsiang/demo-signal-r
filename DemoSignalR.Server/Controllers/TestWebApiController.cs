﻿using DemoSignalR.Server.Chat;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace DemoSignalR.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestWebApiController : ControllerBase
    {


        private readonly ILogger<TestWebApiController> _logger;

        private IHubContext<ChatHub> _context;

        public TestWebApiController(ILogger<TestWebApiController> logger, IHubContext<ChatHub> context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public void GetTestA(string Name)
        {
            string info = $"当前接收到的信息为：{Name},{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}";
            _context.Clients.All.SendAsync("", info);
        }


    }
}
