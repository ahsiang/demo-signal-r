﻿using Microsoft.AspNetCore.SignalR;

namespace DemoSignalR.Server.Chat
{
    public class ChatHub : Hub
    {
        #region 连接和断开连接

        public override async Task OnConnectedAsync()
        {
            var connId = Context.ConnectionId;
            Console.WriteLine($"{connId} 已连接");
            await base.OnConnectedAsync();
        }

        public void StartNotify(string type)
        {
            if (type == "1")
            {

            }
            else if (type == "2")
            {

            };

        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            //如果断开连接
            var connId = Context.ConnectionId;
            Console.WriteLine($"{connId} 已断开");
            await base.OnDisconnectedAsync(ex);
        }

        #endregion
    }
}
