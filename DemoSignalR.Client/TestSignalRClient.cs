﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSignalR.Client
{
    internal class TestSignalRClient : SignalRClient
    {
        public Action<object> RefreshInfos;

        public Action<string> Reconnected;

        public TestSignalRClient() : base()
        {
        }

        public override void Listen()
        {
            HubConnection.On<object>("RefreshInfos", (obj) =>
            {
                //
                if (obj != null)
                {
                    Console.WriteLine("收到数据");
                    //发送消息
                    if (RefreshInfos != null)
                    {
                        RefreshInfos(obj);
                    }
                }
            });
            HubConnection.Reconnected += HubConnection_Reconnected;
        }

        private Task HubConnection_Reconnected(string arg)
        {
            return Task.Run(() =>
            {
                if (Reconnected != null)
                {
                    Reconnected(arg);
                }
            });
        }

        public virtual void StartNotify(string condition)
        {
            HubConnection.SendAsync("StartNotify", condition);
            Console.WriteLine($"开始通过知{condition}");
        }
    }
}
