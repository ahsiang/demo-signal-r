﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoSignalR.Client
{
    internal class SignalRClient
    {
        private readonly HubConnection hubConnection;

        public HubConnection HubConnection
        {
            get { return hubConnection; }
        }

        public SignalRClient()
        {
            var server = ConfigurationManager.AppSettings["Server"].ToString();
            hubConnection = new HubConnectionBuilder().WithUrl($"{server}/chat").WithAutomaticReconnect().Build();
            hubConnection.KeepAliveInterval = TimeSpan.FromSeconds(5);
        }

        public virtual void Listen()
        {

        }

        public async void Start()
        {
            try
            {
                await hubConnection.StartAsync();

            }
            catch (Exception e)
            {

            }
        }
    }
}
